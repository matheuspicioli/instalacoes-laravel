# Instalação PHP #

```sudo apt-add-repository ppa:ondrej/php```

```sudo apt-get update```

```sudo apt-get install php5.6 OU php7.0 OU php7.1```

```sudo apt-get install php5.6-mbstring ou php7.0-mbstring ou php7.1-mbstring```

```sudo apt-get install php5.6-dom ou php7.0-dom ou php7.1-dom```

#### Se der erro com driver de banco de dados (supondo que seja mysql): ####

```sudo apt-get install php5.6-mysql ou php7.0-mysql ou php7.1-mysql```

### Teste com o comando "php -v" ###
O resultado tem que ser parecido com esse:

```Copyright (c) 1997-2017 The PHP Group
Zend Engine v3.1.0, Copyright (c) 1998-2017 Zend Technologies
    with Zend OPcache v7.1.6-1~ubuntu16.04.1+deb.sury.org+1, Copyright (c) 1999-2017, by Zend Technologies```


# Instalação Composer #

```php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"```

```php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"```

```php composer-setup.php```

** Agora precisamos colocar o composer globalmente, para isso execute: **

```sudo mv composer.phar /usr/local/bin/composer```

** Para testar digite: ** ```composer```

# Instalação NodeJS #

```curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -```

```sudo apt-get install -y nodejs```

** Para testar digite: ** ```node -v```

** Exemplo de resultado: ** ```v6.11.0```

### Lembrando que o NPM já vem com o NodeJS ###